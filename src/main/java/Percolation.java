import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/*----------------------------------------------------------------
*  Author:        Pavel Peravoshchykau
*  Written:       10/28/17
*  Last updated:  11/04/17
*
*  Percolation model
*
*----------------------------------------------------------------*/

public class Percolation {

    private final WeightedQuickUnionUF unionUF;
    private final WeightedQuickUnionUF backwashUnionUF;
    private final byte[] nodesArray;
    private final int topIndex;
    private final int bottomIndex;
    private final int n;

    private int openCount;

    public Percolation(int n) {            // create n-by-n grid, with all sites blocked
        if (n <= 0) {
            throw new IllegalArgumentException("n ≤ 0");
        }
        this.n = n;
        int totalSize = n*n;
        topIndex = 0;
        bottomIndex = totalSize + 1;
        unionUF = new WeightedQuickUnionUF(totalSize+1); // UF without bottom node
        backwashUnionUF = new WeightedQuickUnionUF(totalSize+2);
        nodesArray = new byte[totalSize+1];
    }

    public void open(int row, int col) {    // open site (row, col) if it is not open already
        validateArgs(row, col);
        if (isOpen(row, col)) {
            return;
        }
        int index = xyTo1D(row, col);
        nodesArray[index] = 1;
        fullUnion(row, col);
        if (row == 1) {
            unionUF.union(topIndex, index);
            backwashUnionUF.union(topIndex, index);
        } else if (row == n) {
            backwashUnionUF.union(bottomIndex, index);
        }
        openCount++;
    }

    public boolean isOpen(int row, int col) { // is site (row, col) open?
        validateArgs(row, col);
        return nodesArray[xyTo1D(row, col)] > 0;
    }

    public boolean isFull(int row, int col) { // is site (row, col) full?
        validateArgs(row, col);
        return unionUF.connected(xyTo1D(row, col), topIndex);
    }

    public int numberOfOpenSites() {      // number of open sites
        return openCount;
    }

    public boolean percolates() {             // does the system percolate?
        return backwashUnionUF.connected(bottomIndex, topIndex);
    }

    private int xyTo1D(int row, int col) {
        int result = 0;
        if (row > 1) {
            result += n*(row-1);
        }
        result += col;
        return result;
    }

     private void fullUnion(int row, int col) {
         rightUnion(row, col);
         leftUnion(row, col);
         topUnion(row, col);
         bottomUnion(row, col);
     }

     private void leftUnion(int row, int col) {
         if (row > 1) {
             union(row, col, row-1, col);
         }
     }

     private void rightUnion(int row, int col) {
         if (row < n) {
             union(row, col, row+1, col);
         }
     }

     private void topUnion(int row, int col) {
         if (col > 1) {
             union(row, col, row, col-1);
         }
     }

     private void bottomUnion(int row, int col) {
         if (col < n) {
             union(row, col, row, col+1);
         }
     }

    private void union(int rowA, int colA, int rowB, int colB) {
        if (!isOpen(rowB, colB)) {
            return;
        }
        int indexA = xyTo1D(rowA, colA);
        int indexB = xyTo1D(rowB, colB);
        unionUF.union(indexA, indexB);
        backwashUnionUF.union(indexA, indexB);
    }

    private void validateArgs(int row, int col) {
        if (row < 1 || col < 1 || row > n || col > n) {
            throw new IllegalArgumentException("row = "+ row + ", col = "+col);
        }
    }

    public static void main(String[] args) {  // test client (optional)
        Percolation percolation = new Percolation(10);
        percolation.open(2, 2);

    }
}
