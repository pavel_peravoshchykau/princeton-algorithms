import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/*----------------------------------------------------------------
*  Author:        Pavel Peravoshchykau
*  Written:       10/28/17
*  Last updated:  11/04/17
*
*  Class for testing Percolation model
*
*----------------------------------------------------------------*/

public class PercolationStats {

    private static final double CONFIDENCE_95 = 1.96;

    private final double[] percolationDuration;
    private final int trials;
    private double mean;
    private double stddev;

    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("n ≤ 0 or trials ≤ 0");
        }
        this.trials = trials;
        percolationDuration = new double[trials];

        for (int i = 0; i < trials; i++) {
            Stopwatch stopwatch = new Stopwatch();
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int row = StdRandom.uniform(1, n+1);
                int col = StdRandom.uniform(1, n+1);
                if (percolation.isOpen(row, col)) {
                    continue;
                }
                percolation.open(row, col);
            }
            percolationDuration[i] = stopwatch.elapsedTime();
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        if (mean == 0) {
            mean = StdStats.mean(percolationDuration);
        }
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        if (stddev == 0) {
            stddev = StdStats.stddev(percolationDuration);
        }
        return stddev;
    }

    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - (CONFIDENCE_95*Math.sqrt(stddev())/Math.sqrt(trials));
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean() + (CONFIDENCE_95*Math.sqrt(stddev())/Math.sqrt(trials));
    }

    // test client (described below)
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(n, trials);

        System.out.println("mean                    = " + percolationStats.mean());
        System.out.println("stddev                  = " + percolationStats.stddev());
        System.out.println("95% confidence interval = [" + percolationStats.confidenceLo() + ", " + percolationStats.confidenceHi() + "]");
    }
}
