import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private QueueItem<Item> headItem;
    private int size;

    public RandomizedQueue() {             // construct an empty randomized queue

    }

    public boolean isEmpty() {                 // is the randomized queue empty?
        return size == 0;
    }
    
    public int size() {                        // return the number of items on the randomized queue
        return size;
    }

    public void enqueue(Item item) {           // add the item
        checkNull(item);
        QueueItem<Item> newItem = new QueueItem<>(item);
        if (!isEmpty()) {
            newItem.nextItem = headItem;
        }
        headItem = newItem;
        size++;
    }

    public Item dequeue() {                   // remove and return a random item
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        QueueItem<Item> result = null;
        QueueItem<Item> lastItem = null;
        QueueItem<Item> prevItem = null;
        int random = StdRandom.uniform(size);
        for (int i = 0; i < size; i++) {
            if (i == random) {

                result = lastItem == null ? this.headItem : lastItem;

                if (size != 1) {
                    if (prevItem == null) {
                        this.headItem = result.nextItem;
                    } else {
                        prevItem.nextItem = result.nextItem;
                    }
                }

                size--;
                break;
            }

            if (lastItem == null) {
                lastItem = this.headItem;
            } else {
                prevItem = lastItem;
                lastItem = lastItem.nextItem;
            }
        }
        return result.item;

    }

    public Item sample() {                    // return a random item (but do not remove it)
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        QueueItem<Item> result = null;
        QueueItem<Item> lastItem = this.headItem;
        int random = StdRandom.uniform(size);
        for (int i = size-1; i >= random; i--) {
            if (i == random) {
                result = lastItem;
                break;
            }
            lastItem = lastItem.nextItem;
        }
        return result != null ? result.item : null;
    }

    public Iterator<Item> iterator() {        // return an independent iterator over items in random order
        return new RandQIterator();
    }

    private void checkNull(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Null value is not supported.");
        }
    }

    private class QueueItem<T> {
        private final T item;
        private QueueItem<T> nextItem;
        private QueueItem(T item) {
            this.item = item;
        }
    }

    private class RandQIterator implements Iterator<Item> {
        private byte[] randoms = new byte[size];

        private boolean isNew = true;
        private final Item[] items = (Item[]) new Object[size];
        private int attempts;

        public RandQIterator() {
            QueueItem<Item> current = RandomizedQueue.this.headItem;
            for (int i = size-1; i >= 0; i--) {
                items[i] = current.item;
                current = current.nextItem;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }


        @Override
        public boolean hasNext() {
            return attempts < size;
        }

        @Override
        public Item next() {
            if (attempts >= size) {
                throw new NoSuchElementException();
            }
            int random;

            if (isNew) {
                random = StdRandom.uniform(size);
                isNew = false;
            } else {
                do {
                    random = StdRandom.uniform(size);
                } while (randoms[random] == 1);
            }
            randoms[random] = 1;
            attempts++;
            return items[random];
        }
    }


    public static void main(String[] args) {   // unit testing (optional)
        int n = 5;
        RandomizedQueue<Integer> queue = new RandomizedQueue<Integer>();
        for (int i = 0; i < n; i++) {
            queue.enqueue(i);
        }
        for (int a : queue) {
            for (int b : queue)
                StdOut.print(a + "-" + b + " ");
            StdOut.println();
        }

        queue = new RandomizedQueue<Integer>();

        queue.enqueue(1);
        StdOut.println(1 + "=" + queue.dequeue());
        StdOut.println("queue is empty? " + queue.isEmpty());

        queue.enqueue(4);
        StdOut.println(4 + "=" + queue.dequeue());
        StdOut.println("queue is empty? " + queue.isEmpty());

        queue.enqueue(8);
        queue.enqueue(9);
        StdOut.println("8 or 9 -> " + queue.dequeue());
        StdOut.println("queue is empty(false)? " + queue.isEmpty());
        StdOut.println("8 or 9 -> " + queue.dequeue());
        StdOut.println("queue is empty? " + queue.isEmpty());

        queue.enqueue(1);
        queue.enqueue(1);
        queue.enqueue(1);

        queue.enqueue(2);
        queue.enqueue(2);
        queue.enqueue(2);

        queue.enqueue(3);
        queue.enqueue(3);
        queue.enqueue(3);

        int first = 0;
        int second = 0;
        int third = 0;

        while (!queue.isEmpty()) {
            int res = queue.dequeue();
            if (res == 1) {
                first++;
            } else if (res == 2) {
                second++;
            } else if (res == 3) {
                third++;
            }
        }

        StdOut.println("1 was returned " + first + " times.");
        StdOut.println("2 was returned " + second + " times.");
        StdOut.println("3 was returned " + third + " times.");

        try {
            queue.enqueue(null);
        } catch (IllegalArgumentException e) {
            StdOut.println("Expected exception: " + IllegalArgumentException.class.equals(e.getClass()));
        }

        try {
            queue.dequeue();
        } catch (NoSuchElementException e) {
            StdOut.println("Expected exception: " + NoSuchElementException.class.equals(e.getClass()));
        }

        try {
            queue.iterator().remove();
        } catch (UnsupportedOperationException e) {
            StdOut.println("Expected exception: " + UnsupportedOperationException.class.equals(e.getClass()));
        }

    }
}