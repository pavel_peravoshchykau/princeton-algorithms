import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private DequeItem<Item> firstItem;
    private DequeItem<Item> lastItem;
    private int size;

    public Deque() {
        firstItem = new DequeItem<>(null);
        lastItem = firstItem;
    }

    public boolean isEmpty() {                 // is the deque empty?
        return size == 0;
    }

    public int size() {                        // return the number of items on the deque
        return size;
    }

    public void addFirst(Item item) {          // add the item to the front
        checkNull(item);
        if (firstItem.item == null) {
            firstItem.item = item;
        } else {
            DequeItem<Item> newFirstItem = new DequeItem<>(item);
            firstItem.prevItem = newFirstItem;
            newFirstItem.nextItem = firstItem;
            firstItem = newFirstItem;
        }
        ++size;
    }

    public void addLast(Item item) {           // add the item to the end
        checkNull(item);
        if (lastItem.item == null) {
            lastItem.item = item;
        } else {
            DequeItem<Item> newLastItem = new DequeItem<Item>(item);
            lastItem.nextItem = newLastItem;
            newLastItem.prevItem = lastItem;
            lastItem = newLastItem;
        }
        ++size;
    }

    public Item removeFirst() {                // remove and return the item from the front
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item deleted = firstItem.item;

        if (size == 1) {
            firstItem.item = null;
        } else {
            firstItem = firstItem.nextItem;
        }

        --size;
        return deleted;
    }

    public Item removeLast() {                 // remove and return the item from the end
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item deleted = lastItem.item;

        if (size == 1) {
            lastItem.item = null;
        } else {
            lastItem = lastItem.prevItem;
        }

        --size;
        return deleted;
    }

    public Iterator<Item> iterator() {         // return an iterator over items in order from front to end
        return new Iterator<Item>() {
            DequeItem<Item> currentItem = firstItem;
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
            @Override
            public boolean hasNext() {
                return currentItem.nextItem != null;
            }
            @Override
            public Item next() {
                if (currentItem.nextItem == null) {
                    throw new NoSuchElementException();
                }
                currentItem = currentItem.nextItem;
                return currentItem.item;
            }
        };
    }

    private void checkNull(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
    }

    private class DequeItem<T> {
        private T item;
        private DequeItem<T> nextItem;
        private DequeItem<T> prevItem;
        private DequeItem(T item) {
            this.item = item;
        }
    }

    public static void main(String[] args) {   // unit testing (optional)
        Deque<Integer> deque = new Deque<Integer>();
        int n = 5;
        for (int i = 0; i < n; i++)
            deque.addFirst(i);
        for (int a : deque) {
            for (int b : deque)
                StdOut.print(a + "-" + b + " ");
            StdOut.println();
        }

        deque = new Deque<Integer>();

        deque.addFirst(1);
        StdOut.println(1 + "=" + deque.removeLast());
        StdOut.println("deque is empty? " + deque.isEmpty());

        deque.addLast(4);
        StdOut.println(4 + "=" + deque.removeLast());
        StdOut.println("deque is empty? " + deque.isEmpty());

        deque.addFirst(8);
        deque.addFirst(9);
        StdOut.println(8 + "=" + deque.removeLast());
        StdOut.println("deque is empty(false)? " + deque.isEmpty());
        deque.removeLast();


        deque.addLast(1);
        StdOut.println(1 + "=" + deque.removeFirst());
        StdOut.println("deque is empty? " + deque.isEmpty());

        deque.addFirst(7);
        StdOut.println(7 + "=" + deque.removeFirst());
        StdOut.println("deque is empty? " + deque.isEmpty());

        deque.addLast(8);
        deque.addLast(9);
        StdOut.println(8 + "=" + deque.removeFirst());
        StdOut.println("deque is empty(false)? " + deque.isEmpty());
        deque.removeLast();

        try {
            deque.addFirst(null);
        } catch (IllegalArgumentException e) {
            StdOut.println("Expected exception: " + IllegalArgumentException.class.equals(e.getClass()));
        }

        try {
            deque.addLast(null);
        } catch (IllegalArgumentException e) {
            StdOut.println("Expected exception: " + IllegalArgumentException.class.equals(e.getClass()));
        }

        try {
            deque.removeFirst();
        } catch (NoSuchElementException e) {
            StdOut.println("Expected exception: " + NoSuchElementException.class.equals(e.getClass()));
        }

        try {
            deque.removeLast();
        } catch (NoSuchElementException e) {
            StdOut.println("Expected exception: " + NoSuchElementException.class.equals(e.getClass()));
        }

        try {
            deque.iterator().remove();
        } catch (UnsupportedOperationException e) {
            StdOut.println("Expected exception: " + UnsupportedOperationException.class.equals(e.getClass()));
        }
    }
}
