import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DequeTest {
    /*
           public boolean isEmpty()                 // is the deque empty?
           public int size()                        // return the number of items on the deque
           public void addFirst(Item item)          // add the item to the front
           public void addLast(Item item)           // add the item to the end
           public Item removeFirst()                // remove and return the item from the front
           public Item removeLast()                 // remove and return the item from the end
           public Iterator<Item> iterator()         // return an iterator over items in order from front to end
           public static void main(String[] args)   // unit testing (optional)
        }

    Corner cases.  Throw the specified exception for the following corner cases:

        Throw a java.lang.IllegalArgumentException if the client calls either addFirst() or addLast() with a null argument.
        Throw a java.util.NoSuchElementException if the client calls either removeFirst() or removeLast when the deque is empty.
        Throw a java.util.NoSuchElementException if the client calls the next() method in the iterator when there are no more items to return.
        Throw a java.lang.UnsupportedOperationException if the client calls the remove() method in the iterator.
     */

    private Deque<Integer> deque;

    @Before
    public void setUp() {
        deque = new Deque<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddFirst() {
        deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);
        deque.addFirst(4);
        deque.addFirst(5);
        Assert.assertEquals(new Integer(5), deque.removeFirst());
        Assert.assertEquals(4, deque.size());

        Assert.assertEquals(new Integer(4), deque.removeFirst());
        Assert.assertEquals(3, deque.size());

        Assert.assertEquals(new Integer(1), deque.removeLast());
        Assert.assertEquals(2, deque.size());

        Assert.assertEquals(new Integer(2), deque.removeLast());
        Assert.assertEquals(1, deque.size());

        deque.addFirst(null);
    }

    @Test
    public void testRemoveFirst() {
        deque.addLast(1);
        Assert.assertEquals(new Integer(1), deque.removeFirst());
        Assert.assertTrue(deque.isEmpty());

        deque.addFirst(7);
        Assert.assertEquals(new Integer(7), deque.removeFirst());
        Assert.assertTrue(deque.isEmpty());

        deque.addLast(8);
        deque.addLast(9);
        Assert.assertEquals(new Integer(8), deque.removeFirst());
        Assert.assertFalse(deque.isEmpty());
    }

    @Test
    public void testRemoveLast() {
        deque.addFirst(1);
        Assert.assertEquals(new Integer(1), deque.removeLast());
        Assert.assertTrue(deque.isEmpty());

        deque.addLast(4);
        Assert.assertEquals(new Integer(4), deque.removeLast());
        Assert.assertTrue(deque.isEmpty());


        deque.addFirst(8);
        deque.addFirst(9);
        Assert.assertEquals(new Integer(8), deque.removeLast());
        Assert.assertFalse(deque.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddLast() {
        deque.addLast(5);
        deque.addLast(4);
        deque.addLast(3);
        deque.addLast(2);
        deque.addLast(1);
        Assert.assertEquals(new Integer(1), deque.removeLast());
        Assert.assertEquals(4, deque.size());

        Assert.assertEquals(new Integer(2), deque.removeLast());
        Assert.assertEquals(3, deque.size());

        Assert.assertEquals(new Integer(5), deque.removeFirst());
        Assert.assertEquals(2, deque.size());

        Assert.assertEquals(new Integer(4), deque.removeFirst());
        Assert.assertEquals(1, deque.size());

        deque.addFirst(null);
    }

    @Test
    public void testAddFirstAndMultiRemove() {
        deque.addFirst(1);
        Assert.assertEquals(new Integer(1), deque.removeFirst());
        deque.addFirst(4);
        Assert.assertEquals(new Integer(4), deque.removeLast());
        deque.addFirst(5);
        deque.addFirst(6);
        deque.addFirst(7);
        deque.addFirst(8);
        deque.addFirst(9);
        Assert.assertEquals(new Integer(9), deque.removeFirst());
        Assert.assertEquals(new Integer(5), deque.removeLast());
    }

    @Test
    public void testAddLastAndMultiRemove() {
        deque.addLast(2);
        Assert.assertEquals(new Integer(2), deque.removeFirst());
        deque.addLast(3);
        Assert.assertEquals(new Integer(3), deque.removeLast());
        deque.addLast(5);
        deque.addLast(6);
        deque.addLast(7);
        deque.addLast(8);
        deque.addLast(9);
        Assert.assertEquals(new Integer(5), deque.removeFirst());
        Assert.assertEquals(new Integer(9), deque.removeLast());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertTrue(deque.isEmpty());
        deque.addFirst(1);
        Assert.assertTrue(!deque.isEmpty());
        deque.removeLast();
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void testSize() {
        for (int i = 1; i < 1001; i++) {
            deque.addFirst(1);
            Assert.assertEquals(i, deque.size());
        }
        for (int i = 1000; i > 0; i--) {
            deque.removeFirst();
            Assert.assertEquals(i - 1, deque.size());
        }
        Assert.assertEquals(0, deque.size());
        for (int i = 1; i < 1001; i++) {
            deque.addLast(1);
            Assert.assertEquals(i, deque.size());
        }
        for (int i = 1000; i > 0; i--) {
            deque.removeLast();
            Assert.assertEquals(i - 1, deque.size());
        }
        Assert.assertEquals(0, deque.size());
    }
}
