import edu.princeton.cs.algs4.StdOut;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

//public class RandomizesQueueTest {
//    /*
//
//    public class RandomizedQueue<Item> implements Iterable<Item> {
//       public RandomizedQueue()                 // construct an empty randomized queue
//       public boolean isEmpty()                 // is the randomized queue empty?
//       public int size()                        // return the number of items on the randomized queue
//       public void enqueue(Item item)           // add the item
//       public Item dequeue()                    // remove and return a random item
//       public Item sample()                     // return a random item (but do not remove it)
//       public Iterator<Item> iterator()         // return an independent iterator over items in random order
//       public static void main(String[] args)   // unit testing (optional)
//    }
//
//    Iterator.  Each iterator must return the items in uniformly random order. The order of two or more iterators to the same randomized queue must be mutually independent; each iterator must maintain its own random order.
//
//    Corner cases.  Throw the specified exception for the following corner cases:
//
//    Throw a java.lang.IllegalArgumentException if the client calls enqueue() with a null argument.
//    Throw a java.util.NoSuchElementException if the client calls either sample() or dequeue() when the randomized queue is empty.
//    Throw a java.util.NoSuchElementException if the client calls the next() method in the iterator when there are no more items to return.
//    Throw a java.lang.UnsupportedOperationException if the client calls the remove() method in the iterator.
//     */
//
//    private RandomizedQueue<Integer> rQueque;
//
//    public static void main(String[] args) {
//        int n = 5;
//        RandomizedQueue<Integer> queue = new RandomizedQueue<Integer>();
//        for (int i = 0; i < n; i++) {
//            queue.enqueue(i);
//        }
//        for (int a : queue) {
//            for (int b : queue)
//                StdOut.print(a + "-" + b + " ");
//            StdOut.println();
//        }
//    }
//
//    @Before
//    public void setUp() {
//        rQueque = new RandomizedQueue<>();
//    }
//
//    @Test
//    public void testEnqueue() {
//        rQueque.enqueue(1);
//        rQueque.enqueue(2);
//        rQueque.enqueue(3);
//        rQueque.enqueue(4);
//        rQueque.enqueue(5);
//        Assert.assertEquals(5, rQueque.size());
//    }
//
//    @Test
//    public void testDeque() {
//        rQueque.enqueue(1);
//        rQueque.enqueue(2);
//        rQueque.enqueue(3);
//        rQueque.enqueue(4);
//        rQueque.enqueue(5);
//        int res = rQueque.dequeue();
//        Assert.assertEquals(4, rQueque.size());
//    }
//
//    @Test
//    public void testIterator() {
//        List<Integer> intList = Arrays.asList(1,2,3,4,5);
//        for (Integer integer : intList) {
//            rQueque.enqueue(integer);
//        }
//        Assert.assertEquals(intList.size(), rQueque.size());
//        Iterator<Integer> iterator = rQueque.iterator();
//        int count = 0;
//        while (iterator.hasNext()) {
//            intList.remove(iterator.next());
//            ++count;
//        }
//        Assert.assertEquals(intList.size(), rQueque.size());
//    }
//
//    @Test
//    public void testIsEmpty() {
//        Assert.assertTrue(rQueque.isEmpty());
//        rQueque.enqueue(1);
//        Assert.assertFalse(rQueque.isEmpty());
//        rQueque.dequeue();
//        Assert.assertTrue(rQueque.isEmpty());
//    }
//
//    @Test
//    public void testSize() {
//        for (int i = 1; i < 1001; i++) {
//            rQueque.enqueue(1);
//            Assert.assertEquals(i, rQueque.size());
//        }
//        for (int i = 1000; i > 0; i--) {
//            rQueque.dequeue();
//            Assert.assertEquals(i - 1, rQueque.size());
//        }
//        Assert.assertEquals(0, rQueque.size());
//    }
//}
